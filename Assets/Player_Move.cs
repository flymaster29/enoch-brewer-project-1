﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Move : MonoBehaviour {

  public GameObject go;
  public float defaultSpeed;
  private float newSpeed;

	// Use this for initialization
	void Start ()
  {
    GetSpeed((float)1.0);
	}
	
	// Update is called once per frame
	void Update ()
  {
    if (go.tag == "Player_1")
    {
      if (Input.GetKey(KeyCode.W))
      {
        go.transform.Translate((Vector3.up) * newSpeed);
      }

      if (Input.GetKey(KeyCode.S))
      {
        go.transform.Translate((Vector3.down) * newSpeed);
      }
    }

    if (go.tag == "Player_2")
    {
      if (Input.GetKey(KeyCode.UpArrow))
      {
        go.transform.Translate((Vector3.up) * newSpeed);
      }

      if (Input.GetKey(KeyCode.DownArrow))
      {
        go.transform.Translate((Vector3.down) * newSpeed);
      }
    }
  }

  public float GetSpeed(float multiplier)
  {
    newSpeed = defaultSpeed * multiplier;

    return newSpeed;
  }
}
